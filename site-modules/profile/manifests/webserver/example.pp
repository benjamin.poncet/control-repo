class profile::webserver::example (
  String $content1               = "Hello from vhost1\n",
  String $content2               = "Hello from vhost2\n",
  Array[String] $ports           = ['80'],
  Array[Stdlib::Port] $ports_int = [80],
  Array[String] $ips             = ['*'],
) 
{
  class { 'apache':
    default_vhost => false,
  }

  # Vhost 1
  apache::vhost { "vhost1.${fqdn}":
    port => $ports_int,
    ip => $ips,
    ip_based => true,
    docroot => '/var/www/vhost1',
  }
  file { '/var/www/vhost1/index.html':
    ensure  => file,
    content => $content1,
  }

  # Vhost 2
  apache::vhost { "vhost2.${fqdn}":
    port => $ports_int,
    ip => $ips,
    ip_based => true,
    docroot => '/var/www/vhost2',
  }
  file { '/var/www/vhost2/index.html':
    ensure  => file,
    content => $content2,
  }

  firewall { '100 allow http and https access':
    dport  => $ports,
    proto  => tcp,
    action => accept
  }
}